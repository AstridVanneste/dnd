cmake_minimum_required(VERSION 3.9)
project (DND)

set (SOURCE_FILES
        src/Mechanics/Die.cpp
        src/ECS/Components/BaseComponent.cpp)

set (HEADER_FILES
        src/Mechanics/Die.hpp
        src/Mechanics/Abilities.hpp
        src/Mechanics/AbilityTypes.hpp
        src/ECS/Components/BaseComponent.hpp)

add_executable(DND ${SOURCE_FILES} ${HEADER_FILES} src/main.cpp)

target_include_directories(DND PRIVATE src)

target_compile_options(DND
        PRIVATE
        -Wall
        -Wextra
        -Wpedantic)

target_compile_features(DND
        PUBLIC
        cxx_std_17)