//
// Created by Astrid Vanneste on 2019-07-16.
//

#ifndef DND_DIE_HPP
#define DND_DIE_HPP

#include <random>

namespace Mechanics
{
	class Die
	{
		private:
			std::mt19937 rng;
			std::uniform_int_distribution<int> dist;

		public:
			Die() noexcept;
			explicit Die(int type) noexcept;
			explicit Die(Die &die) noexcept;

			long roll() noexcept;
			long roll(long n) noexcept;

	};
}

#endif //DND_DIE_HPP
