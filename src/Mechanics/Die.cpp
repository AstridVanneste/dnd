//
// Created by Astrid Vanneste on 2019-07-16.
//

#include "Die.hpp"

namespace Mechanics
{
	Die::Die() noexcept : Die(20)	// standard die in DND is D20
	{
	}

	Die::Die(int type) noexcept
	{
		std::random_device dev;

		this->rng = std::mt19937(dev());

		this->dist = std::uniform_int_distribution<int>(1, type);
	}

	Die::Die(Die &die) noexcept
	{
		this->rng = die.rng;
		this->dist = die.dist;
	}

	long Die::roll() noexcept
	{
		return this->dist(this->rng);
	}

	long Die::roll(long n) noexcept
	{
		long value = 0;

		for (int i = 0; i < n; i++)
		{
			value += this->roll();
		}

		return value;
	}
}
