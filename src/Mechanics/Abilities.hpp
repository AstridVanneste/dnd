//
// Created by Astrid Vanneste on 2019-07-17.
//

#ifndef DND_ABILITIES_HPP
#define DND_ABILITIES_HPP

#endif //DND_ABILITIES_HPP

namespace Mechanics
{
	struct Abilities
	{
		int str;
		int dex;
		int con;
		int intel;
		int wis;
		int cha;
	};
}