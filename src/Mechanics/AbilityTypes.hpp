//
// Created by Astrid Vanneste on 2019-07-17.
//

#ifndef DND_ABILITYTYPES_HPP
#define DND_ABILITYTYPES_HPP

#endif //DND_ABILITYTYPES_HPP

namespace Mechanics
{
	enum AbilityTypes
	{
		STR,
		DEX,
		CON,
		INTEL,
		WIS,
		CHA
	};
}