//
// Created by Astrid Vanneste on 2019-07-16.
//
#include <iostream>
#include <memory>

#include "Mechanics/Die.hpp"

int main(int argc, char *argv[]){
   std::cout << "Hello World!" << std::endl;

   int type = 20;

   std::shared_ptr<Mechanics::Die> die = std::make_shared<Mechanics::Die>(type);

   std::cout << die->roll() << std::endl;

   return 0;
}

