//
// Created by Astrid Vanneste on 2019-07-17.
//

#ifndef DND_BASECOMPONENT_HPP
#define DND_BASECOMPONENT_HPP

#include <string>

namespace ECS::Components
{
	class BaseComponent
	{
		private:


		protected:
			std::string key;

		public:
			explicit BaseComponent(std::string &key) noexcept;
	};
}


#endif //DND_BASECOMPONENT_HPP
