//
// Created by Astrid Vanneste on 2019-07-17.
//

#include "BaseComponent.hpp"

namespace ECS::Components
{
	BaseComponent::BaseComponent(std::string &key) noexcept
	{
		this->key = key;
	}
}