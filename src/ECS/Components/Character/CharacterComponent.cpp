//
// Created by Astrid Vanneste on 2019-07-17.
//

#include "CharacterComponent.hpp"

namespace ECS::Components
{
	CharacterComponent::CharacterComponent() noexcept : CharacterComponent(0, {10, 10, 10, 10, 10, 10})
	{
	}

	CharacterComponent::CharacterComponent(CharacterComponent &characterComponent) noexcept : CharacterComponent(characterComponent.level, characterComponent.abilities)
	{
	}

	CharacterComponent::CharacterComponent(int level, Mechanics::Abilities abilities) noexcept
	:BaseComponent((std::string &) "character")
	{
		this->level = level;
		this->abilities = abilities;
	}


}
