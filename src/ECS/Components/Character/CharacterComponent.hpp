//
// Created by Astrid Vanneste on 2019-07-17.
//

#ifndef DND_CHARACTERCOMPONENT_HPP
#define DND_CHARACTERCOMPONENT_HPP

#include "Mechanics/Abilities.hpp"
#include "ECS/Components/BaseComponent.hpp"

namespace ECS::Components
{
	class CharacterComponent : BaseComponent
	{
		private:
			int level;

			Mechanics::Abilities abilities;

			CharacterComponent() noexcept;

		public:
			CharacterComponent(int level, Mechanics::Abilities abilities) noexcept;
			CharacterComponent(CharacterComponent &characterComponent) noexcept;

			int check(std::string type);


	};
}


#endif //DND_CHARACTERCOMPONENT_HPP
